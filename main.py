import os
import socket
import threading
from concurrent.futures import ThreadPoolExecutor

import commands
import loader
from maze.maze_create import maze, draw_maze

START_TEXT = (
    "*Команды*\n"
    "1. `start` - Подтверждение игроками готовности к игре. \n"
    "2. `info` - Выводит информацию: в том числе позволяет начать игру после ввода команды`<start>`\n"
    "3. `move` - Позволяет персонажу двигаться нужны атрибуты u, d, r, l\n"
    "Пример команды: `move l` - Перемещает влево\n"
    "4. `k` - Команда для маньяка, которая позволяет убить игрока.\n"
    "5. `a` - Активация кнопки. "
    "После того, как активируются 5 кнопка из 7, открываются две двери через которые игроки смогут сбежать.\n"
)


def handle_command(user_id: int, cmd: str, *args: str) -> str:
    try:
        command_func = loader.commands_dict.get(cmd)
        if command_func is None:
            return "Unknown command\n"
        result = command_func(user_id, *args)
    except (TypeError, ValueError, AssertionError) as e:
        return str(e)
    except StopIteration:
        return ""
    return result


def connect_player(conn, addr):
    print(f"Using thread {threading.get_ident()} for client: {addr}")
    read_file = conn.makefile(mode="r", encoding="utf-8")
    write_file = conn.makefile(mode="w", encoding="utf-8")

    write_file.write(START_TEXT + "Введите команду start\n" + "\n")
    write_file.flush()

    cmd = read_file.readline().strip()
    while cmd:
        result = handle_command(addr, *cmd.split())
        print(draw_maze(maze))
        if result in ["", "Вы спасены\n"] or not commands.dict_players[addr].status:
            if result == "":
                write_file.write("Goodbye!\n")
                write_file.flush()
            else:
                write_file.write("Вы спасены\n")
                write_file.flush()
            break
        write_file.write(result + "\n")
        write_file.flush()
        cmd = read_file.readline().strip()
    conn.close()


def main():
    host = "127.0.0.1"
    port = 8080
    print(f"Started process with PID={os.getpid()}")

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((host, port))
        s.listen(6)
        with ThreadPoolExecutor(max_workers=6) as executor:
            while True:
                conn, addr = s.accept()
                executor.submit(connect_player, conn, addr)


if __name__ == "__main__":
    main()
