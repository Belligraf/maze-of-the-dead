from maze.maze_create import draw_maze


def test_draw_maze():
    maze_draw = draw_maze([["R2", "LDB"], ["UR0", "ULE"]])
    print(maze_draw)
    assert (
        "B" in maze_draw and "2" in maze_draw and "H" in maze_draw and "C" in maze_draw
    )


def test_draw_activate_button():
    assert "+" in draw_maze([["+"]])


def test_draw_open_exit():
    assert "@" in draw_maze([["E@"]])


def test_draw_void():
    assert "*" in draw_maze([[""]])
