from maze.classes import Button, Exit
from maze.func import create_exit
from maze.maze_create import create_buttons, maze, SIZE

commands_dict = {}

buttons: list[Button] = create_buttons(maze)
exits: list[Exit] = create_exit(maze, SIZE - 1)

CLOSE_DOOR: str = "С"
OPEN_DOOR: str = "@"
OFF_BUTTON: str = "B"
ON_BUTTON: str = "+"
REQUIRED_COUNT_BUTTON_ON: int = 5
REQUIRED_COUNT_PLAYERS: int = 5
