import random

from maze.classes import Exit, Player

OFF_SWITCH: str = "B"
EXIT_DESIGNATION: str = "E"


def check_can_kill(players: list[Player]) -> Player or None:
    """
    Can I kill a player?
    """
    for player in players[1:]:
        if players[0].coordinate == player.coordinate and player.status:
            return player
    return None


def kill_player(players: list[Player]) -> Player or None:
    """
    Killing a player
    """
    player_dead = check_can_kill(players)
    if player_dead:
        return player_dead.dead()
    return None


def check_can_create_exit_in_cell(maze: list[list[str]], coordinate: tuple) -> bool:
    """
    Can I make an exit here?
    """
    return (
        OFF_SWITCH in maze[coordinate[0]][coordinate[1]]
        and maze[coordinate[0]][coordinate[1]]
    )


def get_random_cells(size_maze: int) -> tuple:
    """
    Randomize coordinates
    """
    random_cell_x = random.randint(0, size_maze)
    random_cell_y = random.randint(0, size_maze)
    return random_cell_x, random_cell_y


def create_exit(maze: list, size_maze: int) -> list[Exit]:
    """
    Create exit
    """
    exits = []
    count_exit = 2
    random_cell_x, random_cell_y = get_random_cells(size_maze)

    for i in range(count_exit):
        while check_can_create_exit_in_cell(maze, (random_cell_x, random_cell_y)):
            random_cell_x, random_cell_y = get_random_cells(size_maze)
        maze[random_cell_x][random_cell_y] += EXIT_DESIGNATION
        exits.append(Exit(False, (random_cell_x, random_cell_y)))
        random_cell_x, random_cell_y = get_random_cells(size_maze)
    return exits
