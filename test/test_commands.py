import pytest

from commands import (
    register_command,
    start,
    kill,
    activate_button,
    dict_players,
    info,
    move,
)
from loader import commands_dict, buttons, exits
from maze.maze_create import players, draw_maze, maze


def test_register_command():
    @register_command()
    def func():
        return 1

    assert commands_dict["func"]() == 1


def test_custom_name_register_command():
    @register_command("test")
    def func():
        return 1

    assert commands_dict["test"]() == 1


@pytest.mark.parametrize(
    "addr, count_connect", [(1, -1), (2, 0), (3, 1), (4, 2), (5, 3), (6, 4)]
)
def test_start(addr, count_connect):
    count_connect += 1
    if count_connect == 5:
        assert start(addr) == "Игра готова\n"
    else:
        assert start(addr) == f"Ждите подключения еще {5 - count_connect} игроков\n"


def test_kill():
    hunter = players[0]
    player = players[1]
    player.coordinate = hunter.coordinate
    kill(1)
    assert player.status is False


def test_activate_button():
    player = dict_players[1]
    button = buttons[0]
    player.coordinate = button.coordinate
    assert activate_button(1) == "Вы активировали кнопку для побега нужно еще 4\n"


def test_activate_button_and_open_exit():
    player = dict_players[1]
    for i in range(4):
        player.coordinate = buttons[i].coordinate
        activate_button(1)
    player.coordinate = buttons[4].coordinate
    assert (
        activate_button(1) == "Вы активировали последнюю кнопку, двери открыты, БЕГИ\n"
    )


def test_info_for_hunter():
    assert info(1) == "Вы маньяк H\nНачинаем игру!\n" + str(draw_maze(maze))


def test_info_for_player():
    assert info(2) == "Вы игрок 1\nНачинаем игру!\n" + str(draw_maze(maze))


def test_move():
    player = players[0]
    start_coord = player.coordinate
    move(1, "u")
    assert start_coord[0] - player.coordinate[0] == 1


def test_move_exit():
    player = dict_players[2]
    for i in range(5):
        player.coordinate = buttons[i].coordinate
        activate_button(1)
    maze_exit = exits[0]
    player.coordinate = maze_exit.coordinate[0] - 1, maze_exit.coordinate[1]
    assert move(2, "d") == "Вы спасены\n"
