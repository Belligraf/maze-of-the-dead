import functools

from loader import (
    commands_dict,
    buttons,
    exits,
    OPEN_DOOR,
    REQUIRED_COUNT_BUTTON_ON,
    OFF_BUTTON,
    ON_BUTTON,
    REQUIRED_COUNT_PLAYERS,
)
from maze.classes import Player
from maze.func import kill_player
from maze.maze_create import players, maze, draw_maze, COUNT_BUTTONS

COORDINATE_SIDE: dict[str, tuple[int, int]] = dict(
    r=(0, 1), l=(0, -1), u=(-1, 0), d=(1, 0)
)
count_connect: int = -1
count_ready: int = 0
dict_addrs: dict[int, bool] = dict()
dict_num_addr: dict[int, int] = dict()
dict_players: dict[int, Player] = dict()
count_doors: int = 0


def register_command(name: str = None) -> callable:
    def inner(func: callable) -> callable:
        if name is None:
            commands_dict[func.__name__] = func
        else:
            commands_dict[name] = func
        return func

    return inner


def required_args(count: int) -> callable:
    def inner(func: callable) -> callable:
        @functools.wraps(func)
        def wrapped_func(*args, **kwargs):
            if len(args) + len(kwargs) == count:
                if all(map(lambda arg: arg is not None, [*args, *kwargs.values()])):
                    return func(*args, **kwargs)
            raise AssertionError(
                f"{func.__name__} command takes exactly {count - 1} argument(s)\n"
            )

        return wrapped_func

    return inner


@register_command()
@required_args(count=1)
def start(addr: int) -> str:
    global count_connect
    if addr not in dict_addrs:
        dict_num_addr[addr] = count_connect + 1
        dict_addrs[addr] = True
        dict_players[addr] = players[dict_num_addr[addr]]
        count_connect += 1

    if REQUIRED_COUNT_PLAYERS - count_connect:
        return (
            f"Ждите подключения еще {REQUIRED_COUNT_PLAYERS - count_connect} игроков\n"
        )
    else:
        return "Игра готова\n"


@register_command()
@required_args(count=1)
def info(addr: int) -> str:
    if count_connect == REQUIRED_COUNT_PLAYERS:
        print(str(draw_maze(maze)))
        if not dict_num_addr[addr]:
            return "Вы маньяк H\nНачинаем игру!\n" + str(draw_maze(maze))

        else:
            dict_addrs[addr] = False
            return f"Вы игрок {dict_num_addr[addr]}\nНачинаем игру!\n" + str(
                draw_maze(maze)
            )

    return f"В данный момент готов {count_connect + 1} игроков из 6\n"


@register_command()
@required_args(count=2)
def move(addr: int, side: str) -> str:
    x, y = dict_players[addr].coordinate  # int, int
    c_x, c_y = COORDINATE_SIDE[side]  # int, int
    if side.upper() in maze[x][y]:
        maze[x][y]: str = maze[x][y].replace(str(dict_num_addr[addr]), "")
        maze[x + c_x][y + c_y] += str(dict_num_addr[addr])
        dict_players[addr].coordinate = (x + c_x, y + c_y)
        for maze_exit in exits:
            if (
                dict_players[addr].coordinate == maze_exit.coordinate
                and maze_exit.status
                and not dict_players[addr].role
            ):
                maze[x + c_x][y + c_y] = maze[x + c_x][y + c_y].replace(
                    str(dict_num_addr[addr]), ""
                )
                kill_player(players)
                return "Вы спасены\n"

        return str(draw_maze(maze))

    else:
        return "Тупик!\n"


@register_command(name="a")
@required_args(count=1)
def activate_button(addr: int) -> str:
    global count_doors
    x, y = dict_players[addr].coordinate  # int, int
    for i in range(COUNT_BUTTONS):
        if (
            count_doors < REQUIRED_COUNT_BUTTON_ON
            and OFF_BUTTON in maze[x][y]
            and dict_players[addr].coordinate == buttons[i].coordinate
        ):
            buttons[i].activate_button()
            count_doors += 1
            x, y = buttons[i].coordinate
            maze[x][y]: str = maze[x][y].replace(OFF_BUTTON, ON_BUTTON)
            return f"Вы активировали кнопку для побега нужно еще {REQUIRED_COUNT_BUTTON_ON - count_doors}\n"
        elif count_doors >= REQUIRED_COUNT_BUTTON_ON - 1:
            for maze_exit in exits:
                maze_exit.open_exit()
                x_1, y_1 = maze_exit.coordinate
                maze[x_1][y_1] += OPEN_DOOR
            return "Вы активировали последнюю кнопку, двери открыты, БЕГИ\n"
    return "Тут нет кнопки\n"


@register_command(name="k")
@required_args(count=1)
def kill(addrs: int) -> str:
    dead = kill_player(players)
    if isinstance(dead, Player):
        return f"Вы убили игрока {dead.id}\n"
    else:
        return "Здесь никого нет, кроме нас с тобой мой сладкий ❤️\n"
