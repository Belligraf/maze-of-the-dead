class Player:
    """
    Всего игроки (включая маньяка), имеют роль, статус, id и координаты (где они находятся в данный момент)
    """

    def __init__(self, id: int, status: bool, role: bool, coordinate: tuple):
        self.id = id  # 0, 1, 2 etc...
        self.status = status  # true = live
        self.role = role  # hunter or no (true = hunter)
        self.coordinate = coordinate

    def dead(self):
        self.status = False
        return self


class Button:
    """
    Кнопки, имеют id, статус (включена/выключена) и координаты
    """

    def __init__(self, id: int, status: int, coordinate: tuple):
        self.id = id  # 0, 1, 2 etc
        self.status = status
        self.coordinate = coordinate

    def activate_button(self):
        self.status = True


class Exit:
    """
    Выходы, имеют статус (открыт/закрыт) и координаты
    """

    def __init__(self, status: int, coordinate: tuple):
        self.status = status
        self.coordinate = coordinate

    def open_exit(self):
        self.status = True
