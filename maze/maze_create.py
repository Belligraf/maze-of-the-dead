import random
import sys

from maze.classes import Player, Button
from maze.func import OFF_SWITCH, EXIT_DESIGNATION

PATH: list[str] = ["U", "R", "D", "L"]
PLAYERS = ["0", "1", "2", "3", "4", "5"]
SIZE: int = 10
COUNT_BUTTONS = 7
COUNT_PLAYERS = 5
MOVE_UPSTAIRS: str = "U"
MOVE_DOWN: str = "D"
MOVE_RIGHT: str = "R"
MOVE_LEFT: str = "L"
CLOSED_EXIT: str = "C"
ON_SWITCH: str = "+"
WALL: str = "#"
FREE_CELL: str = "."
HUNTER_PLAYER_ID: str = "0"
HUNTER_IN_MAZE: str = "H"
OPEN_EXIT: str = "@"
EMPTY_SPACE: str = "*"


def check_cell(x: int, y: int) -> bool:
    return not (x >= SIZE or y >= SIZE or x < 0 or y < 0)


def mark_path(
    maze: list[list[str]],
    coordinate: tuple,
    previous: str,
    next_el: str,
    c0_n: int = 0,
    c1_n: int = 0,
) -> None:
    # c0_n = coordinate 0 next
    maze[coordinate[0]][coordinate[1]] += previous
    maze[coordinate[0] + c0_n][coordinate[1] + c1_n] += next_el


def get_cells(paths: list, maze: list, visit_maze: list, coordinate: tuple):
    """
    give random cell in we can walk
    """
    if (
        MOVE_UPSTAIRS in paths
        and check_cell(coordinate[0] - 1, coordinate[1])
        and visit_maze[coordinate[0] - 1][coordinate[1]]
    ):
        mark_path(maze, coordinate, MOVE_UPSTAIRS, MOVE_DOWN, c0_n=-1)
        go_to_cell(maze, visit_maze, (coordinate[0] - 1, coordinate[1]))

    if (
        MOVE_RIGHT in paths
        and check_cell(coordinate[0], coordinate[1] + 1)
        and visit_maze[coordinate[0]][coordinate[1] + 1]
    ):
        mark_path(maze, coordinate, MOVE_RIGHT, MOVE_LEFT, c1_n=1)
        go_to_cell(maze, visit_maze, (coordinate[0], coordinate[1] + 1))

    if (
        MOVE_DOWN in paths
        and check_cell(coordinate[0] + 1, coordinate[1])
        and visit_maze[coordinate[0] + 1][coordinate[1]]
    ):
        mark_path(maze, coordinate, MOVE_DOWN, MOVE_UPSTAIRS, c0_n=1)
        go_to_cell(maze, visit_maze, (coordinate[0] + 1, coordinate[1]))

    if (
        MOVE_LEFT in paths
        and check_cell(coordinate[0], coordinate[1] - 1)
        and visit_maze[coordinate[0]][coordinate[1] - 1]
    ):
        mark_path(maze, coordinate, MOVE_LEFT, MOVE_RIGHT, c1_n=-1)
        go_to_cell(maze, visit_maze, (coordinate[0], coordinate[1] - 1))


def go_to_cell(
    maze: list[list[str]], visit_maze: list, coordinate: tuple
) -> list[list[str]]:
    """
    maze generation
    """
    visit_maze[coordinate[0]][coordinate[1]] = False

    count_path = random.randint(1, 4)
    random_paths = [random.choice(PATH) for _ in range(count_path)]
    sort_rand_paths = list(set(random_paths))

    get_cells(sort_rand_paths, maze, visit_maze, coordinate)

    if len(maze[coordinate[0]][coordinate[1]]) <= 1:
        get_cells(PATH, maze, visit_maze, coordinate)

    if len(maze[coordinate[0]][coordinate[1]]) <= 1:
        if check_cell(coordinate[0] - 1, coordinate[1]):
            mark_path(maze, coordinate, MOVE_UPSTAIRS, MOVE_DOWN, c0_n=-1)

        if check_cell(coordinate[0], coordinate[1] + 1):
            mark_path(maze, coordinate, MOVE_RIGHT, MOVE_LEFT, c1_n=1)

        if check_cell(coordinate[0] + 1, coordinate[1]):
            mark_path(maze, coordinate, MOVE_DOWN, MOVE_UPSTAIRS, c0_n=1)

        if check_cell(coordinate[0], coordinate[1] - 1):
            mark_path(maze, coordinate, MOVE_LEFT, MOVE_RIGHT, c1_n=-1)

    return maze


def get_random_cells(restriction_left=0, restriction_right=0) -> tuple:
    """
    Randomize coordinates
    """
    # x, y this coordinate
    x, y = (
        random.randint(0, restriction_left if restriction_left else SIZE - 1),
        random.randint(0, restriction_right if restriction_right else SIZE - 1),
    )
    while not maze[x][y]:
        x, y = (random.randint(0, SIZE - 1), random.randint(0, SIZE - 1))
    return x, y


def distribution_of_players(maze: list) -> list[Player]:
    """
    Distribution of players
    """
    hunter = Player(0, True, True, (SIZE // 2, SIZE // 2))
    players = [hunter]
    for player_id in range(1, COUNT_PLAYERS + 1):
        players.append(
            Player(
                player_id, True, False, get_random_cells(SIZE // 2 - 2, SIZE // 2 + 2)
            )
        )

    for player in players:
        maze[player.coordinate[0]][player.coordinate[1]] += str(player.id)

    return players


def create_buttons(maze: list):
    """
    Create buttons
    """
    buttons = []
    for button_id in range(COUNT_BUTTONS):
        button = Button(
            button_id,
            False,
            get_random_cells(),
        )
        buttons.append(button)
        x, y = button.coordinate
        maze[x][y] += OFF_SWITCH
    return buttons


def draw_maze(maze: list):
    """
    Conclusion the maze in a beautiful way
    """
    x = 0  # coordinate
    y = 0
    size = len(maze)

    res = (size * 2 + 1) * (WALL + " ") + "\n"
    for line in maze:
        for word in line:
            if MOVE_LEFT not in word:
                res += WALL + " "
            else:
                res += FREE_CELL + " "

            for i in PLAYERS:
                if i in word and players[int(i)].status:
                    if i == HUNTER_PLAYER_ID:
                        res += HUNTER_IN_MAZE + " "  # H - Hunter
                        break
                    res += i + " "
                    break
            if res[-2] not in word and res[-2] != HUNTER_IN_MAZE:
                if OFF_SWITCH in word:
                    res += OFF_SWITCH + " "
                elif ON_SWITCH in word:
                    res += ON_SWITCH + " "
                elif EXIT_DESIGNATION in word:

                    if OPEN_EXIT in word:
                        res += OPEN_EXIT + " "  # open
                    else:
                        res += CLOSED_EXIT + " "  # close
                else:
                    res += (FREE_CELL + " ") if word else (EMPTY_SPACE + " ")
            x += 1
        res += WALL + "\n"
        y += 1
        x = 0
        if 0 < y < size:
            for i in range(size):
                if MOVE_DOWN in maze[y - 1][i]:
                    res += WALL + " " + FREE_CELL + " "
                else:
                    res += WALL + " " + WALL + " "
            res += WALL + " \n"

    res += (size * 2 + 1) * (WALL + " ") + "\n"
    return res


sys.setrecursionlimit(64 * 32)
maze: list[list[str]] = [["" for _ in range(SIZE)] for _ in range(SIZE)]
visit_maze: list[list[bool]] = [[True for _ in range(SIZE)] for _ in range(SIZE)]
maze = go_to_cell(maze, visit_maze, (0, 0))
players: list[Player] = distribution_of_players(maze)
