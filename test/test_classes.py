from maze.classes import Player, Button, Exit


def test_kill_player():
    player = Player(1, True, False, (1, 1))
    player.dead()
    assert player.status is False


def test_activate_button():
    button = Button(1, False, (1, 1))
    button.activate_button()
    assert button.status is True


def test_open_exit():
    maze_exit = Exit(False, (1, 1))
    maze_exit.open_exit()
    assert maze_exit.status is True
