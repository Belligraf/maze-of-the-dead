import pytest

from maze.classes import Player
from maze.func import (
    kill_player,
    check_can_kill,
    check_can_create_exit_in_cell,
)

SIZE = 10


@pytest.mark.parametrize(
    "players",
    [
        (
            Player(0, True, True, (SIZE // 2, SIZE // 2)),
            Player(1, True, False, (0, 0)),
            Player(2, True, False, (1, 1)),
            Player(3, True, False, (2, 2)),
            Player(4, True, False, (3, 3)),
            Player(5, True, False, (5, 5)),
        ),
        (
            Player(0, True, True, (0, 2)),
            Player(1, True, False, (0, 0)),
            Player(2, False, False, (1, 7)),
            Player(3, True, False, (7, 7)),
            Player(4, True, True, (0, 2)),
            Player(5, True, False, (9, 9)),
        ),
        (
            Player(0, True, True, (5, 8)),
            Player(1, True, False, (1, 7)),
            Player(2, False, True, (1, 5)),
            Player(3, True, False, (5, 8)),
            Player(4, True, False, (0, 0)),
            Player(5, True, False, (9, 4)),
        ),
        (
            Player(0, True, False, (5, 1)),
            Player(1, True, False, (5, 1)),
            Player(2, True, False, (0, 7)),
            Player(3, True, False, (8, 8)),
            Player(4, True, True, (5, 0)),
            Player(5, True, False, (3, 5)),
        ),
        (
            Player(0, False, True, (5, 5)),
            Player(1, False, False, (0, 1)),
            Player(2, True, False, (1, 0)),
            Player(3, True, False, (7, 7)),
            Player(4, True, True, (5, 5)),
            Player(5, True, False, (5, 4)),
        ),
    ],
)
def test_kill_player_TRUE(players: list[Player]):
    assert isinstance(kill_player(players), Player)


@pytest.mark.parametrize(
    "players",
    [
        (
            Player(0, True, True, (SIZE // 2, SIZE // 2)),
            Player(1, True, False, (0, 0)),
            Player(2, True, False, (1, 1)),
            Player(3, True, False, (2, 2)),
            Player(4, True, False, (3, 3)),
            Player(5, True, False, (7, 7)),
        ),
        (
            Player(0, False, True, (9, 8)),
            Player(1, True, False, (0, 0)),
            Player(2, False, False, (0, 1)),
            Player(3, True, True, (2, 0)),
            Player(4, True, False, (4, 3)),
            Player(5, True, True, (7, 9)),
        ),
        (
            Player(0, False, True, (0, 4)),
            Player(1, True, False, (1, 1)),
            Player(2, True, False, (0, 1)),
            Player(3, True, True, (2, 4)),
            Player(4, True, True, (2, 3)),
            Player(5, True, True, (0, 5)),
        ),
        (
            Player(0, False, True, (5, 9)),
            Player(1, False, False, (1, 1)),
            Player(2, True, False, (0, 7)),
            Player(3, True, False, (2, 4)),
            Player(4, False, True, (2, 1)),
            Player(5, True, True, (5, 5)),
        ),
        (
            Player(0, False, True, (8, 8)),
            Player(1, True, True, (1, 1)),
            Player(2, True, False, (6, 6)),
            Player(3, False, False, (1, 8)),
            Player(4, False, False, (7, 8)),
            Player(5, True, True, (8, 5)),
        ),
    ],
)
def test_kill_player_NONE(players: list[Player]):
    assert kill_player(players) is None


@pytest.mark.parametrize(
    "players",
    [
        (
            Player(0, True, True, (SIZE // 2, SIZE // 2)),
            Player(1, True, False, (0, 0)),
            Player(2, True, False, (1, 1)),
            Player(3, True, False, (2, 2)),
            Player(4, True, False, (3, 3)),
            Player(5, True, False, (5, 5)),
        ),
        (
            Player(0, True, True, (5, 8)),
            Player(1, True, False, (1, 7)),
            Player(2, False, True, (1, 5)),
            Player(3, True, False, (5, 8)),
            Player(4, True, False, (0, 0)),
            Player(5, True, False, (9, 4)),
        ),
        (
            Player(0, False, True, (5, 5)),
            Player(1, False, False, (0, 1)),
            Player(2, True, False, (1, 0)),
            Player(3, True, False, (9, 7)),
            Player(4, True, True, (5, 5)),
            Player(5, True, False, (5, 4)),
        ),
        (
            Player(0, False, True, (9, 0)),
            Player(1, True, False, (0, 1)),
            Player(2, True, False, (1, 1)),
            Player(3, True, True, (9, 8)),
            Player(4, True, True, (9, 0)),
            Player(5, True, False, (5, 1)),
        ),
        (
            Player(0, False, True, (1, 1)),
            Player(1, True, True, (0, 1)),
            Player(2, True, False, (1, 1)),
            Player(3, False, True, (5, 8)),
            Player(4, True, False, (0, 0)),
            Player(5, True, False, (3, 5)),
        ),
    ],
)
def test_check_can_kill_TRUE(players: list[Player]):
    assert isinstance(check_can_kill(players), Player)


@pytest.mark.parametrize(
    "players",
    [
        (
            Player(0, True, True, (SIZE // 2, SIZE // 2)),
            Player(1, True, False, (0, 0)),
            Player(2, True, False, (1, 1)),
            Player(3, True, False, (2, 2)),
            Player(4, True, False, (3, 3)),
            Player(5, True, False, (7, 7)),
        ),
        (
            Player(0, True, True, (SIZE // 2, SIZE // 2)),
            Player(1, True, False, (1, 0)),
            Player(2, False, False, (3, 5)),
            Player(3, False, False, (3, 3)),
            Player(4, True, False, (8, 5)),
            Player(5, False, True, (7, 9)),
        ),
        (
            Player(0, True, True, (SIZE // 2, SIZE // 2)),
            Player(1, False, False, (5, 0)),
            Player(2, False, True, (8, 8)),
            Player(3, False, True, (9, 9)),
            Player(4, True, False, (4, 5)),
            Player(5, False, True, (0, 1)),
        ),
        (
            Player(0, True, True, (SIZE // 2, SIZE // 2)),
            Player(1, True, False, (7, 7)),
            Player(2, True, True, (1, 2)),
            Player(3, False, False, (4, 0)),
            Player(4, True, False, (5, 7)),
            Player(5, False, True, (0, 0)),
        ),
        (
            Player(0, True, True, (SIZE // 2, SIZE // 2)),
            Player(1, True, False, (0, 5)),
            Player(2, False, True, (1, 1)),
            Player(3, False, True, (6, 3)),
            Player(4, True, False, (7, 0)),
            Player(5, True, True, (1, 1)),
        ),
    ],
)
def test_check_can_kill_NONE(players: list[Player]):
    assert kill_player(players) is None


@pytest.mark.parametrize(
    "maze, coordinate",
    [
        ([["R1", "LDB"], ["UR0", "ULE"]], (0, 1)),
        ([["R5", "L"], ["UR0", "ULB"]], (1, 1)),
        ([["R3", "LR"], ["URB", "UL"]], (1, 0)),
        ([["L4", "LRU", "L"], ["UR", "UL", "RB"], ["UR", "UL", "D"]], (1, 2)),
        ([["RL5", "U", "D"], ["R", "ULR", "RU"], ["UR", "ULB", "D"]], (2, 1)),
    ],
)
def test_check_can_create_exit_in_cell_TRUE(maze: list, coordinate: tuple):
    assert check_can_create_exit_in_cell(maze, coordinate)


@pytest.mark.parametrize(
    "maze, coordinate",
    [
        ([["R1", "LDB"], ["UR0", "ULE"]], (1, 1)),
        ([["R5", "DB"], ["R0", "UDR"]], (0, 0)),
        ([["В5", "D4"], ["0U", "R"]], (0, 1)),
        ([["L1", "RL", "L2"], ["U", "UR3", "R"], ["UR", "U", "DB"]], (1, 2)),
        ([["R", "ULRD", "D1"], ["RU", "RB", "RU"], ["UR", "UL", "D5"]], (2, 1)),
    ],
)
def test_check_can_create_exit_in_cell_FALSE(maze: list, coordinate: tuple):
    assert not check_can_create_exit_in_cell(maze, coordinate)
